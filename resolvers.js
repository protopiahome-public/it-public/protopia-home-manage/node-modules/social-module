// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'community' || 'team';

module.exports = {

  Mutation: {
    // changeRole: async (obj, args, ctx, info) => {
    //
    //     let user = await ctx.user;
    //
    //     if(!user)
    //         throw new AuthenticationError('must authenticate');
    //
    //     if(!ctx.access.can(user.role).updateAny(resource).granted)
    //         throw new ForbiddenError('Forbidden');
    //
    //     let role;
    //     if (args.id) {
    //         role = await ctx.db.role.findAndModify({
    //             Query: {_id: new ObjectId(args.id)},
    //             update: {
    //                 $set : {
    //                     title: args.title
    //                 }
    //             }
    //         });
    //     } else {
    //         role = await ctx.db.role.insert({
    //             title: args.title
    //         });
    //     }
    //
    //     return role;
    //
    // },
    changePerson: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'person', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'person', input: args.input }, global.actor_timeout);
    },
    changeCommunity: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'community', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'community', input: args.input }, global.actor_timeout);
    },
    changeTeam: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.input.member_ids) {
        args.input.member_ids = args.input.member_ids.map((e) => new ObjectId(e));
      }

      if (args._id) {
        return await query(collectionItemActor, { type: 'team', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'team', input: args.input }, global.actor_timeout);
    },

    addMemberTeam: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, {
        type: 'team',
        search: { _id: args._id },
        input:
                    {
                      $addToSet: { member_ids: new ObjectId(args.member_id) },
                    },

      }, global.actor_timeout);
    },

    removeMemberTeam: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, {
        type: 'team',
        search: { _id: args._id },
        input:
                    { $pull: { member_ids: new ObjectId(args.member_id) } },
      }, global.actor_timeout);
    },
  },
  Query: {

    // getRole: async (obj, args, ctx, info) => {
    //
    //     let user = await ctx.user;
    //
    //     if(!user)
    //         throw new AuthenticationError('must authenticate');
    //
    //     if(!ctx.access.can(user.role).readAny(resource).granted)
    //         throw new ForbiddenError('Forbidden');
    //
    //     return await ctx.db.role.findOne(
    //         {
    //             _id: new ObjectId(args.id)
    //         }
    //     );
    //
    // },

    // getRoles: async (obj, args, ctx, info) => {
    //
    //     let user = await ctx.user;
    //
    //     if(!user)
    //         throw new AuthenticationError('must authenticate');
    //
    //     if(!ctx.access.can(user.role).readAny(resource).granted)
    //         throw new ForbiddenError('Forbidden');
    //
    //     return await ctx.db.role.find();
    //
    // },

    getTeam: async (obj, args, ctx, info) => {
      let teams = await ctx.db.team.aggregate(
        [
          { $match: { _id: new ObjectId(args.id) } },
          {
            $lookup: {
              from: 'person',
              localField: 'member_ids',
              foreignField: '_id',
              as: 'member',
            },
          },
          { $limit: 1 },
        ],
      );

      let team = teams[0];
      return team || {};

      // return await ctx.db.team.findOne({_id: new ObjectId(args.id)});
    },

    getTeams: async (obj, args, ctx, info) => await ctx.db.team.aggregate(
      [
        { $sort: { name: 1 } },
        {
          $lookup: {
            from: 'person',
            localField: 'member_ids',
            foreignField: '_id',
            as: 'member',
          },
        },
      ],
    ),

    // return await ctx.db.team.find();

    getCommunity: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'community', search: { _id: args._id } }, global.actor_timeout);
    },

    getCommunities: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'community' }, global.actor_timeout);
    },
    getPerson: async (obj, args, context, info) => {
      let user = await context.user;

      let person = await context.db.person.findOne(
        {
          _id: new ObjectId(args.id),
        },
      );

      if (person) {
        person.full_name = `${person.first_name} ${person.second_name}`;
      }

      return person;
    },

    getPersons: async (obj, args, context, info) => {
      let user = await context.user;

      let persons = await context.db.person.aggregate([{ $sort: { first_name: 1, second_name: 1 } }]);
      persons.forEach((person) => { person.full_name = `${person.first_name} ${person.second_name}`; });

      return persons;
    },
  },
  Team: {
    member: async (obj, args, context, info) => {
      if (obj.member) {
        obj.member.forEach((person) => { person.full_name = `${person.first_name} ${person.second_name}`; });
      }
      return obj.member;
    },
  },

};
